<html>
    <head>
        <?php include 'head.php'; ?>
    </head>
    
        <!-- Header - Cabeçalho da página -->
        <header>
      <?php include 'header.php'; ?>
        </header>
        <!-- Section - Corpo da página -->
        <section>
              <?php require_once 'section.php'; ?>
         
        </section>
         
        <!-- Footer - Rodapé da página -->
       <footer>
            <?php include 'footer.php';  ?>
            <script src="js/meu-js.js"></script>
        </footer>
       
    
</html>
