<?php
ini_set('display_errors', 1);
require_once '../Dao/ProdutoDao.php';
$produtoDao = new ProdutoDao();

switch ($_REQUEST['acao']) {
    case 'salvar':
        $produto = [
            'nome' => strtoupper($_REQUEST['nome']),
            'preco' => $_REQUEST['preco'],
            'quantidade' => $_REQUEST['quantidade'],
        ];
        $produtoDao->salvar($produto);
        break;
    case 'excluir':
        echo 'vou excluir';
        break;
    case 'editar':
        echo 'vou editar';
        break;
    default:
        echo 'ação não conhecida';
}
