<html>
<?php
require_once 'head.php';
?>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-sm-12 col-md-6">
                <form action="Controller/ProdutoController.php" method="POST" class="form">
                    <input type="hidden" name="acao" value="salvar">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" placeholder="Nome do produto">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="preco" placeholder="Preço em dólar">
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" name="quantidade" min="1">
                    </div>
                    <input type="submit" value="Cadastrar">
                </form>

            </div>

        </div>
    </div>
</body>

</html>